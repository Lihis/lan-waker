/* waker-wol.c
 *
 * Copyright 2020-2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "waker-wol.h"
#include <glib/gi18n.h>
#include <arpa/inet.h>
#include <glib.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>

static guchar *waker_wol_format_mac(const gchar *mac)
{
	guchar *ret = NULL;

	if (mac == NULL)
		return NULL;

	ret = g_malloc(sizeof(unsigned char) * 6);
	if (ret == NULL) {
		g_printerr("Failed to allocate memory for MAC\n");
		return NULL;
	}

	if (sscanf(mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%*c", &ret[0], &ret[1],
		   &ret[2], &ret[3], &ret[4], &ret[5]) != 6) {
		g_printerr("Failed to parse MAC address\n");
		return NULL;
	}

	return ret;
}

void waker_wol_wake(WakerDevice *device)
{
	g_autofree guchar *mac = NULL;
	gint packet_len = (waker_device_secure_on(device) == NULL ? 102 : 108);
	g_autofree guchar *packet = NULL;
	gint sock_fd;
	void *addr = NULL;
	gsize addr_len;
	gint broadcast = 1;
	struct addrinfo hints;
	struct addrinfo *result = NULL;
	char ip[INET6_ADDRSTRLEN];
	const gchar *secure_on = NULL;

	mac = waker_wol_format_mac(waker_device_mac(device));
	if (mac == NULL) {
		return;
	}

	packet = g_malloc(sizeof(guchar) * packet_len);
	if (packet == NULL) {
		g_printerr("Failed to allocate memory for packet\n");
		return;
	}

	memset(packet, 0xFF, 6);

	for (int i = 1; i <= 16; i++)
		memcpy(packet + (i * 6), mac, sizeof(unsigned char) * 6);

	secure_on = waker_device_secure_on(device);
	if (secure_on != NULL)
		memcpy(packet + 102, (guchar *)secure_on, sizeof(guchar) * 6);

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if (getaddrinfo(waker_device_host(device), NULL, &hints, &result) !=
	    0) {
		g_printerr("Failed to get address info\n");
		freeaddrinfo(result);
		return;
	}

	for (struct addrinfo *iter = result; iter != NULL;
	     iter = iter->ai_next) {
		sock_fd = socket(iter->ai_family, iter->ai_socktype,
				 iter->ai_protocol);
		if (sock_fd < 0) {
			continue;
		}

		if (iter->ai_family == AF_INET) {
			addr = iter->ai_addr;
			addr_len = sizeof(struct sockaddr_in);
			((struct sockaddr_in *)addr)->sin_port =
				htons(waker_device_port(device));
			inet_ntop(AF_INET,
				  &((struct sockaddr_in *)addr)->sin_addr, ip,
				  INET6_ADDRSTRLEN);
		} else if (iter->ai_family == AF_INET6) {
			addr = iter->ai_addr;
			addr_len = sizeof(struct sockaddr_in6);
			((struct sockaddr_in6 *)addr)->sin6_port =
				htons(waker_device_port(device));
			inet_ntop(AF_INET,
				  &((struct sockaddr_in6 *)addr)->sin6_addr, ip,
				  INET6_ADDRSTRLEN);
		}

		break;
	}

	if (sock_fd < 0) {
		g_printerr("No address succeeded\n");
		freeaddrinfo(result);
		return;
	}

	if (setsockopt(sock_fd, SOL_SOCKET, SO_BROADCAST, &broadcast,
		       sizeof(broadcast)) < -1) {
		g_printerr("Failed to setup a broadcast socket.\n");
		close(sock_fd);
		freeaddrinfo(result);
		return;
	}

	g_print(gettext("Sending packet to: %s:%d (%02hhx:%02x:%02x:%02x:%02x:%02x)\n"),
		ip, waker_device_port(device), mac[0], mac[1], mac[2], mac[3],
		mac[4], mac[5]);

	if (sendto(sock_fd, packet, packet_len, 0, (struct sockaddr *)addr,
		   addr_len) < 0)
		g_printerr("Failed to send: %s\n", strerror(errno));

	freeaddrinfo(result);
	close(sock_fd);
}

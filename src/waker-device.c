/* waker-device.c
 *
 * Copyright 2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "waker-device.h"

#include <stdio.h>

struct _WakerDevice {
	GObject parent_instace;

	gchar *name;
	gchar *mac;
	gchar *host;
	guint16 port;
	gchar *secure_on;
};

G_DEFINE_TYPE(WakerDevice, waker_device, G_TYPE_OBJECT)

const gchar *waker_device_name(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));

	return self->name;
}

void waker_device_set_name(WakerDevice *self, const gchar *name)
{
	g_assert(WAKER_IS_DEVICE(self));

	g_free(self->name);
	self->name = g_strdup(name);
}

const gchar *waker_device_mac(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));

	return self->mac;
}

void waker_device_set_mac(WakerDevice *self, const gchar *mac)
{
	g_assert(WAKER_IS_DEVICE(self));

	g_free(self->mac);
	self->mac = g_strdup(mac);
}

const gchar *waker_device_host(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));

	return self->host;
}

void waker_device_set_host(WakerDevice *self, const gchar *host)
{
	g_assert(WAKER_IS_DEVICE(self));

	g_free(self->host);
	self->host = g_strdup(host);
}

guint16 waker_device_port(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));

	return self->port;
}

void waker_device_set_port(WakerDevice *self, guint16 port)
{
	g_assert(WAKER_IS_DEVICE(self));

	self->port = port;
}

const gchar *waker_device_secure_on(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));

	return self->secure_on;
}

void waker_device_set_secure_on(WakerDevice *self, const gchar *secure_on)
{
	g_assert(WAKER_IS_DEVICE(self));

	g_free(self->secure_on);
	self->secure_on = g_strdup(secure_on);
}

void waker_device_update(WakerDevice *self, WakerDevice *from)
{
	waker_device_set_name(self, waker_device_name(from));
	waker_device_set_mac(self, waker_device_mac(from));
	waker_device_set_host(self, waker_device_host(from));
	waker_device_set_port(self, waker_device_port(from));
	waker_device_set_secure_on(self, waker_device_secure_on(from));
}

static void waker_device_finalize(GObject *gobject)
{
	G_OBJECT_CLASS(waker_device_parent_class)->finalize(gobject);
}

static void waker_device_dispose(GObject *gobject)
{
	WakerDevice *self = WAKER_DEVICE(gobject);

	g_clear_pointer(&self->name, g_free);
	g_clear_pointer(&self->mac, g_free);
	g_clear_pointer(&self->host, g_free);
	g_clear_pointer(&self->secure_on, g_free);

	G_OBJECT_CLASS(waker_device_parent_class)->dispose(gobject);
}

static void waker_device_class_init(WakerDeviceClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	gobject_class->finalize = waker_device_finalize;
	gobject_class->dispose = waker_device_dispose;
}

static void waker_device_init(WakerDevice *self)
{
	g_assert(WAKER_IS_DEVICE(self));
}
/* waker-config.c
 *
 * Copyright 2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "waker-config.h"

#include <glib/gstdio.h>

struct _WakerConfig {
	GObject parent_instace;

	GList *devices;
};

G_DEFINE_TYPE(WakerConfig, waker_config, G_TYPE_OBJECT)

#define KEY_MAC "mac"
#define KEY_HOST "host"
#define KEY_PORT "port"
#define KEY_SECURE_ON "secure_on"

static gchar *get_config_filename()
{
	return g_strdup_printf("%s/lan-waker.conf", g_get_user_config_dir());
}

static void waker_config_parse_device(WakerConfig *self, GKeyFile *keyfile,
				      const gchar *name, GError **error)
{
	g_autofree gchar *mac = NULL;
	g_autofree gchar *host = NULL;
	gint port = -1;
	g_autofree gchar *secure_on = NULL;

	g_assert(WAKER_IS_CONFIG(self));

	mac = g_key_file_get_string(keyfile, name, KEY_MAC, error);
	if (!mac) {
		g_warning("Failed to get " KEY_MAC ": %s\n", (*error)->message);
		g_clear_error(error);
	}

	host = g_key_file_get_string(keyfile, name, KEY_HOST, error);
	if (!host) {
		g_warning("Failed to get " KEY_HOST ": %s\n",
			  (*error)->message);
		g_clear_error(error);
	}

	port = g_key_file_get_integer(keyfile, name, KEY_PORT, error);
	if (*error) {
		g_warning("Failed to get " KEY_PORT ": %s\n",
			  (*error)->message);
		g_clear_error(error);
	}

	if (g_key_file_has_key(keyfile, name, KEY_SECURE_ON, NULL)) {
		secure_on = g_key_file_get_string(keyfile, name, KEY_SECURE_ON,
						  error);
		if (*error) {
			g_warning("Failed to get " KEY_SECURE_ON ": %s\n",
				  (*error)->message);
			g_clear_error(error);
		}
	}

	if (mac && host && (port >= 0 && port <= G_MAXUINT16)) {
		WakerDevice *device = NULL;

		device = g_object_new(WAKER_TYPE_DEVICE, NULL);
		waker_device_set_name(device, name);
		waker_device_set_mac(device, mac);
		waker_device_set_host(device, host);
		waker_device_set_port(device, port);
		waker_device_set_secure_on(device, secure_on);

		if (waker_config_add(self, device) == FALSE)
			g_warning("Failed to add %s", name);
	}
}

gboolean waker_config_load(WakerConfig *self)
{
	g_autofree gchar *config_file = NULL;
	g_autoptr(GKeyFile) keyfile = NULL;
	g_autoptr(GError) error = NULL;
	gboolean ret = FALSE;

	g_assert(WAKER_IS_CONFIG(self));

	config_file = get_config_filename();
	keyfile = g_key_file_new();

	ret = g_key_file_load_from_file(keyfile, config_file, G_KEY_FILE_NONE,
					&error);
	if (ret) {
		GStrv groups = NULL;
		gsize groups_len = 0;

		groups = g_key_file_get_groups(keyfile, &groups_len);
		for (gsize i = 0; i < groups_len; i++) {
			waker_config_parse_device(self, keyfile, groups[i],
						  &error);
		}
	}

	return TRUE;
}

gboolean waker_config_save(WakerConfig *self)
{
	gboolean ret = FALSE;
	g_autoptr(GKeyFile) keyfile = NULL;
	g_autoptr(GError) error = NULL;
	g_autofree gchar *config_file = NULL;

	g_assert(WAKER_IS_CONFIG(self));

	config_file = get_config_filename();
	keyfile = g_key_file_new();

	for (GList *iter = g_list_first(self->devices); iter;
	     iter = iter->next) {
		WakerDevice *device = WAKER_DEVICE(iter->data);
		const gchar *name = waker_device_name(device);
		const gchar *secure_on = NULL;

		g_key_file_set_string(keyfile, name, KEY_MAC,
				      waker_device_mac(device));
		g_key_file_set_string(keyfile, name, KEY_HOST,
				      waker_device_host(device));
		g_key_file_set_integer(keyfile, name, KEY_PORT,
				       waker_device_port(device));
		secure_on = waker_device_secure_on(device);
		if (secure_on)
			g_key_file_set_string(keyfile, name, KEY_SECURE_ON,
					      secure_on);
	}

	ret = g_key_file_save_to_file(keyfile, config_file, &error);
	if (!ret)
		g_warning("Failed to save configuration: %s", error->message);

	return ret;
}

gboolean waker_config_add(WakerConfig *self, WakerDevice *device)
{
	gboolean ret = TRUE;
	const gchar *name = NULL;

	g_assert(WAKER_IS_CONFIG(self));

	name = waker_device_name(device);
	if (waker_config_find_device(self, name) != NULL)
		ret = FALSE;
	else
		self->devices = g_list_append(self->devices, device);

	return ret;
}

gboolean waker_config_edit(WakerConfig *self, const gchar *old_name,
			   WakerDevice *new_device)
{
	WakerDevice *old_device = NULL;

	g_assert(WAKER_IS_CONFIG(self));

	for (GList *iter = self->devices; iter; iter = iter->next) {
		WakerDevice *device = iter->data;

		if (strcmp(waker_device_name(device), old_name) == 0) {
			old_device = device;
			break;
		}
	}

	if (old_device != NULL) {
		waker_device_update(old_device, new_device);
	}

	return (old_device != NULL ? TRUE : FALSE);
}

gboolean waker_config_remove(WakerConfig *self, const gchar *name)
{
	WakerDevice *device = NULL;

	g_assert(WAKER_IS_CONFIG(self));

	device = waker_config_find_device(self, name);
	if (device == NULL)
		return FALSE;

	self->devices = g_list_remove(self->devices, device);
	g_object_unref(device);

	return TRUE;
}

WakerDevice *waker_config_find_device(WakerConfig *self, const gchar *name)
{
	WakerDevice *device = NULL;

	for (GList *iter = self->devices; iter != NULL; iter = iter->next) {
		WakerDevice *d = iter->data;

		if (strcmp(waker_device_name(d), name) == 0) {
			device = d;
			break;
		}
	}

	return device;
}

GList *waker_config_devices(WakerConfig *self)
{
	g_assert(WAKER_IS_CONFIG(self));

	return self->devices;
}

static void waker_config_finalize(GObject *gobject)
{
	G_OBJECT_CLASS(waker_config_parent_class)->finalize(gobject);
}

static void waker_config_dispose(GObject *gobject)
{
	WakerConfig *self = WAKER_CONFIG(gobject);

	g_list_free_full(self->devices, g_object_unref);
	self->devices = NULL;

	G_OBJECT_CLASS(waker_config_parent_class)->dispose(gobject);
}

static void waker_config_class_init(WakerConfigClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
	gobject_class->finalize = waker_config_finalize;
	gobject_class->dispose = waker_config_dispose;
}

static void waker_config_init(WakerConfig *self)
{
	g_assert(WAKER_IS_CONFIG(self));
}
/* main.c
 *
 * Copyright 2020-2022 Tomi Lähteenmäki
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <adwaita.h>

#include "waker-defs.h"
#include "waker-window.h"
#include "waker-wol.h"
#include "waker-application.h"

int main(int argc, char *argv[])
{
	g_autoptr(WakerConfig) config;
	g_autoptr(WakerApplication) app = NULL;

	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

	config = g_object_new(WAKER_TYPE_CONFIG, NULL);

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--wake") == 0) {
			if (i + 1 <= argc) {
				WakerDevice *device;

				if (waker_config_load(config) == FALSE) {
					g_printerr(_(
						"Failed to load configuration.\n"));
					return 1;
				}

				device = waker_config_find_device(config,
								  argv[i + 1]);
				if (device == NULL) {
					g_printerr(
						gettext("Device \"%s\" does not exist.\n"),
						argv[i + 1]);
					return 1;
				}

				waker_wol_wake(device);
				g_print(_("%s woken.\n"),
					waker_device_name(device));
				return 0;
			}
		}
	}

	app = waker_application_new(config);

	return g_application_run(G_APPLICATION(app), argc, argv);
}
